class example {
  package { 'apache2':
    ensure => installed,
  }

  service { 'apache2':
    ensure => running,
    enable => true,
  }

  file { '/var/www/html/index.html':
    ensure  => file,
    content => 'Hello, Puppet and Terraform!',
    require => Package['apache2'],
    notify  => Service['apache2'],
  }
}
