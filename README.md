# PuppetTerraformIntegration

This repository demonstrates the integration of Terraform and Puppet for automating the configuration management of infrastructure resources.

## Table of Contents

- [Introduction](#introduction)
- [Repository Structure](#repository-structure)
- [Infrastructure Provisioning with Terraform](#infrastructure-provisioning-with-terraform)
- [Configuration Management with Puppet](#configuration-management-with-puppet)
- [Integration](#integration)
- [Documentation](#documentation)
- [Testing](#testing)
- [Submission](#submission)

## Introduction

This repository showcases the automation of configuration management using Puppet for infrastructure provisioned with Terraform. The goal is to ensure that provisioned resources are configured according to the desired state.

## Repository Structure

The repository is organized into the following directories:

- **terraform:** Contains Terraform configuration files for infrastructure provisioning.
- **puppet:** Contains Puppet manifests and modules for configuring provisioned resources.

## Infrastructure Provisioning with Terraform

1. **AWS EC2 Instance Provisioning:**
   - Terraform code in the `terraform` directory provisions a simple AWS EC2 instance.
   - Update `terraform/main.tf` with your AWS credentials and configuration.

## Configuration Management with Puppet

1. **Puppet Manifests:**
   - Puppet manifests are located in the `puppet` directory.
   - Edit `puppet/init.pp` to include tasks like installing packages, configuring system settings, and starting/enabling services.

## Integration

1. **Triggering Puppet Configuration:**
   - The integration is achieved using a Terraform provisioner in `terraform/main.tf`.
   - The provisioner triggers Puppet runs on provisioned instances after Terraform provisioning.

2. **Alternative Method:**
   - Alternatively, you can utilize a configuration management tool like Ansible or Shell scripts within Terraform to execute Puppet runs remotely.

## Documentation

1. **Prerequisites:**
   - Ensure you have valid AWS credentials for Terraform.
   - Make sure Puppet is installed on the provisioned instances.

2. **Installation Steps:**
   - Clone this GitLab repository: `git clone <repository-url>`
   - Navigate to the `terraform` directory: `cd terraform`
   - Initialize Terraform: `terraform init`
   - Apply Terraform configuration: `terraform apply`

3. **Configuration Details:**
   - Update AWS credentials and configuration in `terraform/main.tf`.
   - Modify Puppet manifests in `puppet/init.pp` for specific configurations.

## Testing

1. **Provisioning and Configuration:**
   - Run Terraform to provision infrastructure: `terraform apply`
   - Verify Puppet successfully configures provisioned resources.

2. **Validation:**
   - SSH into provisioned instances to check the configuration state.


