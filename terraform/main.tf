provider "aws" {
  region = "us-east-1"
}

resource "aws_instance" "demo-server" {
    ami = "ami-0440d3b780d96b29d"
    instance_type = "t2.micro"
    key_name = "keypair"

  tags = {
    Name = "puppetagent1"
  }

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("keypair.pem")
    host        = self.public_ip
    timeout     = "5m" 
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt update",
      "sudo wget https://apt.puppetlabs.com/puppet8-release-bionic.deb",
      "sudo dpkg -i puppet8-release-bionic.deb",
      "sudo apt update",
      "sudo apt install puppet-agent -y",
      "sudo sh -c 'echo \"193.53.13.178 puppet\" >> /etc/hosts'",
      "sudo systemctl enable puppet",
      "sudo systemctl start puppet",
      "sudo /opt/puppetlabs/bin/puppet agent --test"


    ]
  }
}

output "public_ip" {
  value = aws_instance.puppetagent1.public_ip
}
